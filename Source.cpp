#include <cmath>
#include <string>
#include "XLibrary11.hpp"

using namespace std;
using namespace XLibrary11;

enum Mode
{
	Title, Game
};

int Main()
{
	const int blockCount = 10;
	const float blockSpace = 200.0f;
	const float speed = 2.0f;

	float stagePosition = 0.0f;
	float distance = 0.0f;
	float fallingSpeed = 0.0f;
	int score = 0;
	Mode mode = Title;

	Window::SetTitle(L"はばたけ鳥");

	Camera camera;
	camera.color = Float4(0.5f, 0.75f, 1.0f, 1.0f);

	Sprite playerSprite(L"player.png");
	playerSprite.scale = 3.0f;

	Sprite blockSprite(L"block.png");
	blockSprite.scale = 5.0f;

	Text scoreText(L"0", 10.0f);
	scoreText.position.y = 200.0f;
	scoreText.scale = 10.0f;
	scoreText.color = Float4(1.0f, 0.0f, 0.0f, 1.0f);

	Text titleText(L"はばたけ鳥", 80.0f);
	titleText.position.y = 100.0f;
	titleText.scale = 1.0f;
	titleText.color = Float4(1.0f, 0.0f, 0.0f, 1.0f);

	Sound flySound(L"fly.wav");
	Sound hitSound(L"hit.wav");
	Sound pointSound(L"point.wav");

	Float2 blockPositions[blockCount];

	while (Refresh())
	{
		camera.Update();

		switch (mode)
		{
		case Title:

			if (Input::GetKeyDown(VK_SPACE))
			{
				stagePosition = 0.0f;
				distance = 0.0f;
				fallingSpeed = 0.0f;
				distance = 0.0f;
				score = 0;
				mode = Game;
				scoreText.Create(L"0", 10.0f);

				for (int i = 0; i < blockCount; i++)
				{
					blockPositions[i] = Float2(stagePosition + blockSpace, Random::Range(-100.0f, 100.0f));
					stagePosition = blockPositions[i].x;
				}
			}

			playerSprite.position = Float3(0.0f, 0.0f, 0.0f);
			playerSprite.angles.z = 0.0f;
			playerSprite.Draw();

			titleText.Draw();

			scoreText.Draw();

			break;

		case Game:

			for (int i = 0; i < blockCount; i++)
			{
				if (blockPositions[i].x < -500.0f)
				{
					blockPositions[i] = Float2(stagePosition + blockSpace, Random::Range(-100.0f, 100.0f));
					stagePosition = blockPositions[i].x;
				}

				if (blockPositions[i].x - 60.0f < playerSprite.position.x &&
					blockPositions[i].x + 60.0f > playerSprite.position.x)
				{
					if (blockPositions[i].y - 80.0f > playerSprite.position.y ||
						blockPositions[i].y + 80.0f < playerSprite.position.y)
					{
						mode = Title;
						hitSound.Play();
					}
				}

				blockPositions[i].x -= speed;

				blockSprite.position = blockPositions[i] + Float2(0.0f, 300.0f);
				blockSprite.Draw();

				blockSprite.position = blockPositions[i] + Float2(0.0f, -300.0f);
				blockSprite.Draw();
			}

			stagePosition -= speed;

			fallingSpeed -= 0.5f;

			if (Input::GetKeyDown(VK_SPACE))
			{
				fallingSpeed = 10.0f;
				flySound.Play();
			}

			playerSprite.position.y += fallingSpeed;
			playerSprite.angles.z = fallingSpeed * 5.0f;
			playerSprite.Draw();

			distance += speed;

			if (distance > blockSpace)
			{
				distance -= blockSpace;
				score++;
				scoreText.Create(to_wstring(score), 10.0f);
				pointSound.Play();
			}

			scoreText.Draw();

			break;
		}
	}

	return 0;
}
